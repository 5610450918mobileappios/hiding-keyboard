//
//  Page3ViewController.swift
//  Hiding Keyboard
//
//  Created by Teerapat on 11/24/2558 BE.
//  Copyright © 2558 Teerapat. All rights reserved.
//

import UIKit

class Page3ViewController: UIViewController ,UITextFieldDelegate{
    @IBOutlet weak var textField: UITextField!

    @IBOutlet weak var slidingView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottonConstraint: NSLayoutConstraint!
    @IBAction func tapAction(sender: AnyObject) {
        self.textField.resignFirstResponder()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardHide:", name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardShow:", name: UIKeyboardWillShowNotification, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func keyboardHide(n:NSNotification) {
        self.topConstraint.constant = 0
        self.bottonConstraint.constant = 0
        self.view.layoutIfNeeded()
        
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.textField.resignFirstResponder()
        return true
    }
    
    func keyboardShow(n:NSNotification){
        let d = n.userInfo!
        var r = (d[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let navHeight:CGFloat = 0
        let keyHeight = r.size.height + navHeight
        
        for subView in self.slidingView.subviews{
            if subView.isFirstResponder(){
                                if subView.isKindOfClass(UITextView){
                                    let textView = subView as! UITextView
                                    let keyboardToDetailTextRec = textView.convertRect(r, fromView:nil)
                                    let frame = textView.frame
                                    let offset = frame.height - keyboardToDetailTextRec.minY
                                    r = textView.convertRect(r, fromView: nil)
                                    textView.contentInset.bottom = offset
                                    textView.scrollIndicatorInsets.bottom = offset
                                    
                
                                }
                if subView.isKindOfClass(UITextField){
                    let textField = subView as! UITextField
                    let viewHeight = self.view.frame.height
                    let textFieldBottom = textField.frame.height+textField.frame.origin.y
                    let topKey = viewHeight - keyHeight
                    let y : CGFloat = textFieldBottom - topKey
                    if topKey <= textFieldBottom {
                        self.topConstraint.constant = -y
                        self.bottonConstraint.constant = y
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
