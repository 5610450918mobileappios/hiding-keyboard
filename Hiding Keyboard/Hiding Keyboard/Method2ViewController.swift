//
//  Method2ViewController.swift
//  Hiding Keyboard
//
//  Created by Teerapat on 11/24/2558 BE.
//  Copyright © 2558 Teerapat. All rights reserved.
//

import UIKit

class Method2ViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var textArea: UITextView!
    @IBOutlet weak var textField: UITextField!
    @IBAction func tapAction(sender: AnyObject) {
        self.textField.resignFirstResponder()
        self.textArea.resignFirstResponder()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.textField.resignFirstResponder()
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
