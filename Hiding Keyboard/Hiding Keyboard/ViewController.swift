//
//  ViewController.swift
//  Hiding Keyboard
//
//  Created by Teerapat on 11/24/2558 BE.
//  Copyright © 2558 Teerapat. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var topTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.topTextField.addTarget(nil, action: "dummy", forControlEvents: .EditingDidEndOnExit)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

